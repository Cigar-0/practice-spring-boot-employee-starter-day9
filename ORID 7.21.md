## ORID

### O:

1.First of all, I conducted a code review of yesterday's JPA transformation work this morning. Through the team member's review, I also found the problem of duplicate code in my code, so I modified it.

2.I learned mapper and dto, encapsulated the original data into transmitted data, and then used mapper to filter or process the data.

3.After a Retro review of the previous learning, the group and the teacher discussed the problems and solutions in the learning process.

4.Learned basic use of flyway. It can be used quickly and efficiently to iterate over database table structures and ensure that data tables remain consistent when deployed to test or production environments.

5.I demonstrated microservices with my team members.

### R:

I feel that today is very fulfilling, and I have learned a lot of knowledge that I have not touched before.

### I:

I also gained a new understanding of microservices by preparing the presentation.

### D:

I will continue to learn spring boot.