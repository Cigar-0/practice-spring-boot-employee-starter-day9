package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return jpaEmployeeRepository.findAll().stream().map(employee -> EmployeeMapper.toResponse(employee)).collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new));
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        EmployeeResponse toBeUpdatedEmployee = findById(id);
        Employee employee = EmployeeMapper.toEntity(toBeUpdatedEmployee);
        if (employeeRequest.getSalary() != null) {
            employee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            employee.setAge(employeeRequest.getAge());
        }
        jpaEmployeeRepository.save(employee);
    }

    public List<Employee> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender);
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee saveEmployee= jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(saveEmployee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findAll(PageRequest.of(page-1, size)).getContent();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
