## ORID

### O:

  1.we carried out the code review of yesterday's spring boot three-layer this morning, and I still don't know how to write integration tests and unit tests.

   2.Learned SQL basic, For example, add, delete, modify and check the operation

3. Learned the basics of jpa, See the relationship between JPA, Hibernate, and Spring Data JPA.

   4.practice to change the repository method to a jpa implementation.

### R:

I think today's lesson is very rich, I feel that learning better than yesterday's experience.

### I:

Using JPA makes it easy and safe to access and manipulate databases. 

### D:

I need to do more practice on unit testing.